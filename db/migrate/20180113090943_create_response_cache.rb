class CreateResponseCache < ActiveRecord::Migration[5.1]
  def change
    create_table :response_caches do |t|
      t.text :json_content
      t.text :query_params
    end
  end
end
