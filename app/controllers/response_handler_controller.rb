class ResponseHandlerController < ApplicationController

  def beer_index
    uri = URI('https://api.punkapi.com/v2/beers')
    @http = Net::HTTP.new(uri.host, uri.port)
    @http.use_ssl = true

    request = Net::HTTP::Get.new("#{uri.path}#{filters_query}")
    @response = sort_response(get_json_response(request))
    cache_response

    render :index
  end

  private

  def filters_query
    return @filters_query if @filters_query || params['filter'].nil?
    query = params.permit!['filter'].merge(per_page: 80).select{|_k, val| val.present?}.to_query
    @filters_query = "?#{query}"
  end

  def sort_response(response)
    return response unless params[:sort]
    response.sort(&sort_block)
  end

  def get_json_response(request)
    JSON.parse(@http.request(request).body)
  end

  def cache_response
    ResponseCache.create(json_content: @response, query_params: filters_query)
  end

  def sort_block
    return proc { |y, x| (x[params[:sort]] <=> y[params[:sort]]) } if params[:direction] == 'desc'
    proc { |x, y| (x[params[:sort]] <=> y[params[:sort]]) }
  end

end
