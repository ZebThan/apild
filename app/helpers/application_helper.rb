module ApplicationHelper

  def sort_direction(sort)
    return 'desc' if params[:sort] == sort && params[:direction] == 'asc'
    'asc'
  end
end
